package INF102.lab1.triplicate;

import java.util.Collections;
import java.util.List;

//<T extends Comparable<T>> inspired by FastDuplicateFinder.java lecture code
public class MyTriplicate<T extends Comparable<T>> implements ITriplicate<T> {

    @Override
    public T findTriplicate(List<T> list) {
        //sort the list
        Collections.sort(list);
        //get the size of the list
        int n = list.size();
        //list.size()-2 because we need to check the last 3 elements and avoid index out of bounds
        try {
            for (int i = 0; i < n; i++) {
                if (list.get(i).equals(list.get(i + 1)) && (list.get(i).equals(list.get(i + 2)))) {
                    return list.get(i);
                }
            }
        } catch (IndexOutOfBoundsException e) {


        }
        return null;
    }

}
